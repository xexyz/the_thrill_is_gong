from socketIO_client import SocketIO
import subprocess
import curses

def on_trade_create(*args):
        subprocess.Popen(['omxplayer','-b','gong.mp3'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        print(args[0])

def on_rfq_create(*args):
        if args[0]['active']:
                subprocess.Popen(['omxplayer','-b','sg.mp3'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        else:
                subprocess.Popen(['omxplayer','-b','wah.mp3'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        print(args[0])

def on_connect(*args):
        subprocess.Popen(['omxplayer','-b','ooh.mp3'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        print('[Connected]')
def on_disconnect(*args):
        print('[disConnected]')
def on_reconnect(*args):
        subprocess.Popen(['omxplayer','-b','ooh.mp3'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        print('[reconnected]')
def on_error(*args):
        print('[error]')

def main(screen):
    curses.initscr()
    curses.noecho()
    curses.cbreak()
    socketIO = SocketIO('URI HERE')
    socketIO.on('connect', on_connect)
    socketIO.on('reconnect', on_reconnect)
    socketIO.on('disconnect', on_disconnect)
    socketIO.on('error', on_error)

    #socketIO.on('monitor/status updated', on_status_update)
    socketIO.on('monitor/trades created', on_trade_create)
    socketIO.on('monitor/rfqs created', on_rfq_create)

    begin_x = 20; begin_y = 7
    height = 5; width = 40
    win = curses.newwin(height, width, begin_y, begin_x)
    socketIO.wait()
    curses.nocbreak()
    curses.echo()
    curses.endwin()

if __name__ == '__main__':
    curses.wrapper(main)
